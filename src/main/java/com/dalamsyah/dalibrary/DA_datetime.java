package com.dalamsyah.dalibrary;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Dimas Alamsyah on 7/17/2019.
 */
public class DA_datetime {

    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    public final String formatYYYYMMdd = "yyyy-MM-dd";

    public DA_datetime(){
        calendar = Calendar.getInstance();
    }

    public String getDateWithFormat(String format){
        simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        return simpleDateFormat.format(calendar.getTime());
    }

    public String convertDateWithFormat(String date, String format, String toFormat) {
        String rtn = "";
        try {
            simpleDateFormat = new SimpleDateFormat(format, Locale.US);
            Date temp = simpleDateFormat.parse(date);
            simpleDateFormat = new SimpleDateFormat(toFormat, Locale.US);
            rtn = simpleDateFormat.format(temp);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return rtn;
    }

    public String getTimeFormat(){
        return "";
    }

}
