package com.SFAWingsV2.libs.ut;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class UT_surface_thread extends Thread {
	// StartRegion Global
	boolean mRun;
	Canvas mcanvas;
	SurfaceHolder surfaceHolder;
	Context context;
	UT_map msurfacePanel;
	 
	public UT_surface_thread(SurfaceHolder sholder, Context ctx, UT_map spanel){
		surfaceHolder = sholder;
		context = ctx;
		mRun = false;
		msurfacePanel = spanel;
	}
	 
	void setRunning(boolean bRun){
		mRun = bRun;
	}
	
	@Override
	public void run(){
		super.run();
		while(mRun){
			mcanvas = surfaceHolder.lockCanvas();
			if(mcanvas != null){
				msurfacePanel.doDraw(mcanvas);
				surfaceHolder.unlockCanvasAndPost(mcanvas);
			}
		}
	}
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
