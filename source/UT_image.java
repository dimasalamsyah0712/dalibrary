package com.SFAWingsV2.libs.ut;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.SFAWingsV2.libs.rs.C;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class UT_image {
	// StartRegion Global
public String file_path = "";
	
	public UT_image(String sub_folder, boolean delete) {
		this.file_path = Environment.getExternalStorageDirectory() + C.image_folder + sub_folder;
		if(!delete) {
			checkMainFolder();
			checkSubFolder();
		}
	}
	
	public UT_image(){}
	
	public void checkMainFolder() {
		File folder = new File(Environment.getExternalStorageDirectory() + C.image_folder);
		if(!folder.exists()) {
			folder.mkdir();
		}
	}
	
	public void checkSubFolder() {
		File folder = new File(file_path);
		if(!folder.exists()) {
			folder.mkdir();
		}
	}
	
	public int storeImage(String image_byte, String file_name) {
		int rtn = 0;
		try {
			File imageFile = new File(file_path, file_name);
			Bitmap image = BitmapFactory.decodeStream(new ByteArrayInputStream(Base64.decode(image_byte, 0)));

            if(image.getByteCount() != 0) {
                OutputStream outStream = new FileOutputStream(imageFile);
                image.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                Log.d("IMG", "SIZE " + (image.getByteCount() / 1000) + " KB FILE NAME " + file_name);
                outStream.flush();
                outStream.close();
            }
			
			rtn = 1;
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}
	
	public boolean deleteAllImage() {
		boolean rtn = false;
		try {
			File folder = new File(file_path);
			if(folder.exists() && folder.isDirectory()) {
				String[] images = folder.list();
				int size = images.length;
				for(int i = 0; i < size; i++) {
					File image = new File(folder, images[i]);
					if(image.isFile()) {
						image.delete();
					}
				}
				
				rtn = folder.delete();
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}
	
	public boolean deleteImage(String file_name) {
		boolean rtn = false;
		try {
			File file = new File(file_path, file_name);
			if(file.exists() && file.isFile()) {
				rtn = file.delete();
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}
	
	public String getUri(String path) {
		return file_path + File.separator + path;
	}
	
	public Bitmap getSalesImage(String sls_code) {
		Bitmap rtn = null;
		try {
			String pathName = file_path + File.separator + sls_code + C.image_extension;
			rtn = BitmapFactory.decodeFile(pathName);
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}
	
	public Bitmap getBitmap(String path){
		Bitmap rtn = null;
		
		try{
			String pathName = file_path + File.separator + path;
			rtn = BitmapFactory.decodeFile(pathName);
		}catch(Exception e){
			new UT_exception_handler().onExceptionHandler(e);
		}
		
		return rtn;
	}
	
	public Bitmap rotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}
	
	public boolean saveBitmap(Bitmap bitmap, String file_name){
		boolean rtn = false;
		
		try {
			File imageFile = new File(file_path, file_name);
			
			OutputStream outStream = new FileOutputStream(imageFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outStream);
			outStream.flush();
            outStream.close();
			
			rtn = true;
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		
		return rtn;
	}
	
	public boolean saveBitmap(Bitmap bitmap, File destination){
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
		FileOutputStream fo;
		try {
			destination.createNewFile();
			
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
			
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public Bitmap compressBitmap(String path, int reqWidth, int reqHeight) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		
		BitmapFactory.decodeFile(path, options);

		final int height = options.outHeight;
		final int width = options.outWidth;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		int inSampleSize = 1;

		if (height > reqHeight) {
			inSampleSize = Math.round((float) height / (float) reqHeight);
		}

		int expectedWidth = width / inSampleSize;

		if (expectedWidth > reqWidth) {
			inSampleSize = Math.round((float) width / (float) reqWidth);
		}

		options.inSampleSize = inSampleSize;

		options.inJustDecodeBounds = false;

		return BitmapFactory.decodeFile(path, options);
	}
	
	public String getImage(String file_name) {
		String rtn = "";
		
		try {
			File imageFile = new File(file_path, file_name);
			Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
			
    		ByteArrayOutputStream stream = new ByteArrayOutputStream();
    		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
    		byte[] byte_arr = stream.toByteArray();
    		rtn = Base64.encodeToString(byte_arr, 0);
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}
	
	public ArrayList<String> getListFolderFile() {
		ArrayList<String> rtn = new ArrayList<String>();
		try {
			File folder = new File(file_path);
			if(folder.exists() && folder.isDirectory()) {
				String[] images = folder.list();
				int size = images.length;
				for(int i = 0; i < size; i++) {
					rtn.add(images[i]);
				}
			}
			
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}
	
	public ArrayList<String> getListFolderFile(String prefix) {
		ArrayList<String> rtn = new ArrayList<String>();
		try {
			File folder = new File(file_path);
			if(folder.exists() && folder.isDirectory()) {
				String[] images = folder.list();
				int size = images.length;
				for(int i = 0; i < size; i++) {
					if(images[i].contains(prefix))
						rtn.add(images[i]);
				}
			}
			
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}	
	
	public int getCountFolderFile(String prefix) {
		int rtn = 0;
		try {
			File folder = new File(file_path);
			if(folder.exists() && folder.isDirectory()) {
				String[] images = folder.list();
				int size = images.length;
				for(int i = 0; i < size; i++) {
					if(images[i].contains(prefix))
						rtn++;
				}
			}
			
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

    public Bitmap addLayer(Bitmap bitmap, String[] texts, int shadow_color, int text_color, int position, int pad_x, int pad_y, int pad_bottom, float font_size) {
        try {
            if (texts != null && texts.length > 0){
                int pos_x = 0;
                int pos_y = 0;

                Paint paint = new Paint();  //set the look
                paint.setAntiAlias(true);
                paint.setColor(text_color);
                paint.setStyle(Style.FILL);
                paint.setShadowLayer(2.0f, 1.0f, 1.0f, shadow_color);

                int pictureHeight = bitmap.getHeight();
                //paint.setTextSize(pictureHeight * .01629f);
                paint.setTextSize(pictureHeight * font_size);

                Canvas canvas = new Canvas(bitmap);
                if (position == C.photo_layer_center) {
                    pos_x = canvas.getWidth()/2;
                    pos_y = canvas.getHeight() - (pad_y * texts.length) - pad_bottom;
                } else if (position == C.photo_layer_right) {
                    pos_x = canvas.getWidth() - pad_x;
                    pos_y = canvas.getHeight() - (pad_y * texts.length) - pad_bottom;
                } else if (position == C.photo_layer_left) {
                    pos_x = pad_x;
                    pos_y = canvas.getHeight() - (pad_y * texts.length) - pad_bottom;
                }

                for (int i = 0; i < texts.length; i++){
                    pos_y += pad_y;
                    canvas.drawText(texts[i] , pos_x, pos_y, paint);
                }
            }
        } catch (Exception ex){
            new UT_exception_handler().onExceptionHandler(ex);
        }

        return bitmap;

    }
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor
	// <used-in="Soap::Supervisor;IceCream::Sales;Soap::Sales"/>
	public Bitmap Y002_AddLayer(Bitmap bitmap, String[] texts, int shadow_color, int text_color, int position, int pad_x, int pad_y, int pad_bottom, float font_size) {
		try {
			if (texts != null && texts.length > 0){
		    	int pos_x = 0;
		    	int pos_y = 0;
		    	
			    Paint paint = new Paint();  //set the look
			    paint.setAntiAlias(true);
			    paint.setColor(text_color);
			    paint.setStyle(Style.FILL);
			    paint.setShadowLayer(2.0f, 1.0f, 1.0f, shadow_color);
			    
			    int pictureHeight = bitmap.getHeight();
		    	//paint.setTextSize(pictureHeight * .01629f);
			    paint.setTextSize(pictureHeight * font_size);
			    
		    	Canvas canvas = new Canvas(bitmap);
		        if (position == C.Y002_photo_layer_center) {
		        	pos_x = canvas.getWidth()/2;
		        	pos_y = canvas.getHeight() - (pad_y * texts.length) - pad_bottom; 
		        } else if (position == C.Y002_photo_layer_right) {
		        	pos_x = canvas.getWidth() - pad_x;
		        	pos_y = canvas.getHeight() - (pad_y * texts.length) - pad_bottom;    
		        } else if (position == C.Y002_photo_layer_left) {
		        	pos_x = pad_x;
		        	pos_y = canvas.getHeight() - (pad_y * texts.length) - pad_bottom;  
		        }   
		        
		        for (int i = 0; i < texts.length; i++){
		        	pos_y += pad_y;
		        	canvas.drawText(texts[i] , pos_x, pos_y, paint);
		        }
			}
		} catch (Exception ex){
			new UT_exception_handler().onExceptionHandler(ex);
		}
		
    	return bitmap;
    	
	}
	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
} 