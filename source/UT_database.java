package com.SFAWingsV2.libs.ut;

import android.os.Environment;

import com.SFAWingsV2.libs.rs.C;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class UT_database {
	// StartRegion Global
	private String pathApp = "";
	private String pathStorage = "";
	
	public UT_database() {
		pathApp = Environment.getDataDirectory().getPath() + "/data/com.SFAWingsV2/databases/sfa.db";
		//pathStorage = Environment.getExternalStorageDirectory().getPath() + "/sfa.db";
		pathStorage = Environment.getExternalStorageDirectory().getPath() + "/";
	}
	
	public String backupDB(boolean manual, String app_date) {
		UT_date_time ut_date = new UT_date_time();
		String file_name = "";
		if(manual) {
			file_name = C.db_manual + "_" + ut_date.convertyyyyMMddtoyyMMdd(app_date) + C.db_ext;
		} else {
			file_name = C.db_auto + "_" + ut_date.convertyyyyMMddtoyyMMdd(app_date) + C.db_ext;
		}
		checkFileBackup(manual, ut_date.convertyyyyMMddtoyyMMdd(app_date), file_name);
		
		return copyDatabase(pathApp, pathStorage + file_name);
	}
	
	public String restoreDB(String file_name) {
		return copyDatabase(pathStorage + file_name, pathApp);
	}
	
//	public String copyDatabase(String fromLocation, String toLocation){
//    	String rtn = "";
//    	
//    	try{
//    		File from = new File(fromLocation);
//    		File to = new File(toLocation);
//    		File copy = new File(to, from.getName());
//    		copy.createNewFile();
//    		
//    		FileChannel inChannel = new FileInputStream(from).getChannel();
//    		FileChannel outChannel = new FileOutputStream(copy).getChannel();
//    		try {
//    			inChannel.transferTo(0, inChannel.size(), outChannel);
//    		}
//    		finally {
//    			if (inChannel != null)
//    				inChannel.close();
//    			if (outChannel != null)
//    				outChannel.close();
//    		}
//    		
//	    	
//    	} catch(Exception e) {
//    		rtn = e.getMessage();
//    	  
//    	}
//		
//    	return rtn;
//    }
	
	public String copyDatabase(String fromLocation, String toLocation){
    	String rtn = "";
    	FileInputStream fis = null;
    	FileOutputStream fos = null;
    	
    	try{
			File f = new File(fromLocation);

			if(f.exists()) {
		    	byte[] buffer = new byte[1024];	    	
	        	
		    	fis = new FileInputStream(f);
		    	fos = new FileOutputStream(toLocation);    	  
	    	  
		    	while(true) {
		    		int i = fis.read(buffer);
		    	    if(i > 0) {
		    	    	fos.write(buffer,0,i);
		    	    } else {
		    	    	break;
		    	    }
		    	}
		    	fos.flush();
		    	
		    	rtn = "Proses berhasil.";
			} else {
		    	rtn = "File tidak ditemukan.";
			}
	    	
    	} catch(Exception e) {
    	  rtn = e.getMessage();
    	  
    	} finally {
    		try{
    			if(fos != null) fos.close();
    			if(fis != null) fis.close();
    		} catch(IOException ioe) {
    			
    		}
    	}
		
    	return rtn;
    }
	
	public String[] getListFile() {
		String[] rtn = new String[0];
		try {
			File folder = new File(pathStorage);
			if(folder.isDirectory()) {
				File[] folder_file = folder.listFiles();
				ArrayList<String> list_file = new ArrayList<String>();
				for(File file : folder_file) {
					if(file.isFile()) {
						String file_name = file.getName();
						if(file_name.endsWith(C.db_ext)) {
							list_file.add(file_name);
						}
					}
				}
				
				rtn = new String[list_file.size()];
				int i = 0;
				for(String file_name : list_file) {
					rtn[i] = file_name;
					i++;
				}
				
				Arrays.sort(rtn);
			}
    	} catch(Exception e) {
    		e.printStackTrace();
		}
		return rtn;
	}
	
	private void checkFileBackup(boolean manual, String app_date, String file_name) {
		try {
			String[] list_file = getListFile();
			int count = 0;
			ArrayList<String> file_date = new ArrayList<String>();
			
			for(String existing : list_file) {
				if(manual) {
					if(existing.startsWith(C.db_manual + "_")) {
						count++;
						file_date.add(existing);
					}
				} else {
					if(existing.startsWith(C.db_auto + "_")) {
						count++;
						file_date.add(existing);
					}
				}
			}
			count++;
			file_date.add(file_name);
			
			Collections.sort(file_date, new Comparator<String>() {
		        @Override
		        public int compare(String s1, String s2) {
		            return s1.compareToIgnoreCase(s2);
		        }
		    });
			
			if(count > C.db_limit) {
				while(file_date.size() > C.db_limit) {
					File current = new File(pathStorage + file_date.get(0));
					if(current.exists() && current.isFile()) {
						current.delete();
					}
					file_date.remove(0);
				}
			}
			
			File current = new File(pathStorage + file_name);
			if(current.exists() && current.isFile()) {
				current.delete();
			}
    	} catch(Exception e) {
    		e.printStackTrace();
		}
	}
	// EndRegion
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
	
	// StartRegion IceCream::Supervisor
	public String Y005_backupDB(boolean manual, String app_date) {
		UT_date_time ut_date = new UT_date_time();
		String file_name = "";
		if(manual) {
			file_name = C.Y005_db_manual + "_" + ut_date.convertyyyyMMddtoyyMMdd(app_date) + C.db_ext;
		} else {
			file_name = C.Y005_db_auto + "_" + ut_date.convertyyyyMMddtoyyMMdd(app_date) + C.db_ext;
		}
		checkFileBackup(manual, ut_date.convertyyyyMMddtoyyMMdd(app_date), file_name);
		
		return copyDatabase(pathApp, pathStorage + file_name);
	}
	// EndRegion

}
