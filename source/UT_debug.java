/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */

package com.SFAWingsV2.libs.ut;

import android.util.Log;

import com.SFAWingsV2.libs.rs.C;

public class UT_debug implements C{
	// StartRegion Global
public UT_debug(){};
	
	public void logging(char log_type, String title, String tag, Object message){		
		switch(log_type){
			case 'e' :
				Log.e(title, tag + " : " + message.toString());
				break;
			case 'd' :
				Log.d(title, tag + " : " + message.toString());
				break;
			case 'i' :
				Log.i(title, tag + " : " + message.toString());
				break;
			case 'v' :
				Log.v(title, tag + " : " + message.toString());
				break;
			case 'w' :
				Log.w(title, tag + " : " + message.toString());
				break;
			default  :
				Log.wtf(title, tag + " : " + message.toString());
				break;
		}
	}
	
	public void e(Object message){	
		Log.e("SFAWingsV2", "SFAWingsV2 : " + message.toString());
	}
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
