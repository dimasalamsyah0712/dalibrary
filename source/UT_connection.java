package com.SFAWingsV2.libs.ut;

import android.content.Context;
import android.net.ConnectivityManager;

import com.SFAWingsV2.libs.be.BE_connection;
import com.SFAWingsV2.libs.be.BE_message;
import com.SFAWingsV2.libs.bl.BL_t_scan_cust;
import com.SFAWingsV2.libs.rs.C;
import com.SFAWingsV2.libs.sp.SP_setting;

import java.lang.reflect.Method;

public class UT_connection {
	// StartRegion Global
	private Context context;
	
	public UT_connection(Context context) {
		this.context = context;
	}
	
	public boolean isConnected() {
		boolean wifi_con = false;
		boolean mobile_con = false;
		
    	ConnectivityManager connection = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	
    	android.net.NetworkInfo wifi = connection.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    	if(wifi != null && wifi.isConnected()) {
    		wifi_con = true;
    	}
    	
    	android.net.NetworkInfo mobile = connection.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
    	if(mobile != null && mobile.isConnected()) {
    		mobile_con = true;
    	}
    	
		return wifi_con || mobile_con;
	}
	
	public boolean isWiFiConnection() {
		boolean rtn = false;
    	ConnectivityManager connection = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	android.net.NetworkInfo wifi = connection.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    	if(wifi.isConnected()) {
    		rtn = true;
    	}
		
		return rtn;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean hasMobileConnection() {
		boolean rtn = false;
    	ConnectivityManager connection = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//    	android.net.NetworkInfo mobile = connection.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
//    	if(mobile != null && mobile.isConnectedOrConnecting()) {
//			rtn = true;
//		}
	
    	try {
            Class cmClass = Class.forName(connection.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            rtn = (Boolean)method.invoke(connection);
            
        } catch (Exception e) {
            // Some problem accessible private API
            // TODO do whatever error handling you want here
        	android.net.NetworkInfo mobile = connection.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        	if(mobile != null && mobile.isConnectedOrConnecting()) {
    			rtn = true;
    		}
        }
    	
		return rtn;
	}
	// EndRegion
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion 
	
	// StartRegion ModernTrade::Sales
	public BE_message Y005_check_loc_con(String cust_id) {
		BE_message rtn = new BE_message();
    	try {
			UT_location_service ut_location = new UT_location_service(context);
			boolean location = ut_location.isGPSEnabled();
			boolean connection = hasMobileConnection();
			if(location && connection) {
				rtn.success = true;
			} else {
				rtn.success = false;
				BL_t_scan_cust bl_scan = new BL_t_scan_cust(context);
				rtn.message.add(bl_scan.Y005_updateLocationConnection(location, connection, cust_id));
			}
        } catch (Exception e) {
        	new UT_exception_handler().onExceptionHandler(e);
        }
    	return rtn;
	}
	// EndRegion

    //FS11625
    public BE_connection getConnection(){
        BE_connection rtn = new BE_connection();

        try {
            SP_setting setting = new SP_setting(context);
            String webservice_setting = setting.readWebservice();

            if(!webservice_setting.equals("")) {
                rtn.webservice = webservice_setting;
            } else {
                rtn.webservice = C.Y003_webservice;
            }
            rtn.namespace = C.Y003_namespace;

        } catch (Exception ex){
            new UT_exception_handler().onExceptionHandler(ex);
        }

        return rtn;
    }

    //FS12398
    public BE_connection getConnection(String namespace, String webservice){
        BE_connection rtn = new BE_connection();

        try {
            SP_setting setting = new SP_setting(context);
            String webservice_setting = setting.readWebservice();

            if(!webservice_setting.equals("")) {
                rtn.webservice = webservice_setting;
            } else {
                rtn.webservice = webservice;
            }
            rtn.namespace = namespace;

        } catch (Exception ex){
            new UT_exception_handler().onExceptionHandler(ex);
        }

        return rtn;
    }
}
