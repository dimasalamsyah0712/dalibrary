/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */

package com.SFAWingsV2.libs.ut;

public interface UT_task_manager{
	// StartRegion Global
	public void onTaskFinished(Object object);
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
