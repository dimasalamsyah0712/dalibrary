/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */

package com.SFAWingsV2.libs.ut;

public class UT_exception_handler{	
	// StartRegion Global
	public UT_exception_handler(){}
	
	public void onExceptionHandler(Exception e){
		android.util.Log.e("SFAWingsV2", "SFAWingsV2 : " + e.getMessage());
		e.printStackTrace();
	}
	
	public void onExceptionHandler(Exception e, String scoope){
		android.util.Log.e("SFAWingsV2", "SFAWingsV2 ( " + scoope + " ) : " + e.getMessage());
		e.printStackTrace();
	}
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
