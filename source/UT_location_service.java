/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */

package com.SFAWingsV2.libs.ut;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

import com.SFAWingsV2.ui.Activity_Base;

public class UT_location_service extends Service implements LocationListener {
    // StartRegion Global
    private Context context;
    private LocationManager lmGPS;
    //	private double longitudeGPS = 100.652437;
//  private double latitudeGPS = -0.774418;
    private double longitudeGPS;
    private double latitudeGPS;

    //    private boolean isGPSEnabled;
//    private boolean isNetworkEnabled;
    private LocationManager lmInet;
    private double longitudeInet;
    private double latitudeInet;
    private double accuracy;

    public UT_location_service(Context context) {
        this.context = context;
    }

    public double getLongitude() {
        if (longitudeInet != 0) {
            return this.longitudeInet;
        } else {
            return this.longitudeGPS;
        }
    }

    public double getLatitude() {
        if (latitudeInet != 0) {
            return this.latitudeInet;
        } else {
            return this.latitudeGPS;
        }
    }

//	public void initLocation() {
//		if(lmGPS == null) {
//			lmGPS = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
//	        //lmGPS.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListenerGPS);
//	        lmGPS.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, this);
//	        locationGPS = lmGPS.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//			try {
//				if(locationGPS != null) {
//					longitudeGPS = locationGPS.getLongitude();
//					latitudeGPS = locationGPS.getLatitude();
//				} else {
//					lmGPS.removeUpdates(this);
//					//lmGPS.removeUpdates(locationListenerGPS);
//				}
//			} catch (Exception ex) { }
//		}
//		
//		if(lmInet == null) {
//			lmInet = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
//			//lmInet.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 10, locationListenerInet);
//			lmInet.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 10, this);
//	        locationInet = lmInet.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//			try {
//				if(locationInet != null) {
//					longitudeInet = locationInet.getLongitude();
//					latitudeInet = locationInet.getLatitude();
//				} else {
//					lmInet.removeUpdates(this);
//					//lmInet.removeUpdates(locationListenerInet);
//				}
//			} catch (Exception ex) { }
//		}
//	}

    public void initLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }

        if (lmGPS == null) {
            lmGPS = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            lmGPS.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 0, locationListenerGPS);
		}
		
		if(lmInet == null) {
			lmInet = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
			lmInet.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 0, locationListenerInet);
		}
		
	}
	
	public void stopLocation() {
		if(lmGPS != null) {
			lmGPS.removeUpdates(locationListenerGPS);
		}
		if(lmInet != null) {
			lmInet.removeUpdates(locationListenerInet);
		}
	}
	
    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };
	
	private final LocationListener locationListenerInet = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeInet = location.getLongitude();
            latitudeInet = location.getLatitude();
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

//	@Override
//	public void onLocationChanged(Location location) {
//		SharedPreferences.Editor writer = context.getSharedPreferences(C.shared_preferences, C.shared_preferences_mode).edit();
//		if(latitudeGPS != 0 && longitudeGPS != 0) {
//			writer.putLong(V.sfa_latitude, Double.doubleToLongBits(latitudeGPS));
//			writer.putLong(V.sfa_longitude, Double.doubleToLongBits(longitudeGPS));
//		} else {
//			writer.putLong(V.sfa_latitude, Double.doubleToLongBits(latitudeInet));
//			writer.putLong(V.sfa_longitude, Double.doubleToLongBits(longitudeInet));
//		}
//		writer.commit();
//	}

	@Override
	public void onLocationChanged(Location location) {
        longitudeGPS = location.getLongitude();
        latitudeGPS = location.getLatitude();
        lmGPS.removeUpdates(this);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* {+FS6631 */
	public boolean isGPSEnabled() {
		boolean rtn = false;
		if(lmGPS == null) {
			lmGPS = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
	        rtn = lmGPS.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}
		return rtn;
	}
	
	public double getHaversineInKM(double lat1, double long1, double lat2, double long2) {
	    double _eQuatorialEarthRadius = 6378.1370D;
	    double _d2r = (Math.PI / 180D);
	    
        double dlong = (long2 - long1) * _d2r;
        double dlat = (lat2 - lat1) * _d2r;
        double a = Math.pow(Math.sin(dlat / 2D), 2D) + Math.cos(lat1 * _d2r) * Math.cos(lat2 * _d2r)
                * Math.pow(Math.sin(dlong / 2D), 2D);
        double c = 2D * Math.atan2(Math.sqrt(a), Math.sqrt(1D - a));
        double d = _eQuatorialEarthRadius * c;

        return d;
    }
	
	public double getHaversineInM(double lat1, double long1, double lat2, double long2) {
		return (1000D * getHaversineInKM(lat1, long1, lat2, long2));
    }

    public double getAccuracy() {
        return this.accuracy;
    }
	/* } */
	
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
