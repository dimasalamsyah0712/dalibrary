/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */

package com.SFAWingsV2.libs.ut;

import com.SFAWingsV2.libs.rs.C;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/** Changes List
 * @changes : FSXX.17112016 - Penambahan informasi customer group & jadwal kunjungan
 * @author  : Fransisca Caroline
 * @created : 2016.11.17
 */

public class UT_date_time {
	// StartRegion Global
	private Calendar calendar;
	private SimpleDateFormat simpleDateFormat;

	public static final String date_format_yyyyMMdd = "yyyy-MM-dd";
	public static final String date_format_ddMMyyyy = "dd-MM-yyyy";
	public static final String date_format_ddMMMyyyy = "dd MMM yyyy";
	public static final String date_format_yyyyMMdd_HHmmsss = "yyyy-MM-dd HH:mm:ss";
	public static final String date_format_ddMMyyyy_HHmmsss = "dd-MM-yyyy HH:mm:ss";
    public static final String date_format_yyMMdd_HHmmsss = "yyMMddHHmmss";
	public static final String date_format_HHmm = "HH:mm";
	public static final String date_format_HHmmss = "HH:mm:ss";
	public static final String date_format_HHmmss_SSS = "HH:mm:ss.SSS";
	public static final String date_format_a = "a";
	public static final String date_format_EEEE_dd_LLLL_yyyy = "EEEE, dd LLLL yyyy";
	public static final String date_format_dd_LLLL_yyyy = "dd LLLL yyyy";
	public static final String date_format_yyMMdd = "yyMMdd";
	public static final String date_format_yyyyMM = "yyyyMM";
	public static final String date_format_MM = "MM";
	public static final String date_format_yyyy = "yyyy";
	public static final String date_format_MMMyyyy = "MMM yyy";
	public static final String date_format_yyMMddHHmm = "yyMMddHHmm";
	public static final String date_format_HHmmss2 = "HHmmss";
	public static final String date_format_yyyyMMdd_HHmmsss_photo = "dd MMM yyy HH:mm:ss";
	public static final String date_format_yyyyMMddnoStrip = "yyyyMMdd";

	public UT_date_time(){
		calendar = Calendar.getInstance();
	}

	public UT_date_time(Calendar calendar){
		this.calendar = calendar;
	}

	public String getDateFormat(String dateFormat){
		simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentyyyyMMdd(){
		simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentyyMMdd(){
		simpleDateFormat = new SimpleDateFormat(date_format_yyMMdd, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentddMMyyyyHHmmss(){
		simpleDateFormat = new SimpleDateFormat(date_format_ddMMyyyy_HHmmsss, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentyyyyMMddHHmmss(){
		simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd_HHmmsss, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentDeviceDateTime(){
		simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd_HHmmsss, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentHHmmss(){
		simpleDateFormat = new SimpleDateFormat(date_format_HHmmss, Locale.US);
		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentHHmmss_SSS(){
		simpleDateFormat = new SimpleDateFormat(date_format_HHmmss_SSS, Locale.US);
		return simpleDateFormat.format(calendar.getTime());
	}

	public String getCurrentyyyyMMddnoStrip(){
		simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMddnoStrip, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getBackUpTime(){
		simpleDateFormat = new SimpleDateFormat(date_format_HHmmss2, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String getHHMM(){
		simpleDateFormat = new SimpleDateFormat(date_format_HHmm, Locale.US);
		return simpleDateFormat.format(calendar.getTime());
	}

	public String getAmPm(){
		simpleDateFormat = new SimpleDateFormat(date_format_a, Locale.US);
		return simpleDateFormat.format(calendar.getTime());
	}

	public String getEEEEddLLLLyyyy(){
		simpleDateFormat = new SimpleDateFormat(date_format_EEEE_dd_LLLL_yyyy, Locale.US);
		return simpleDateFormat.format(calendar.getTime());
	}

	public String getddLLLLyyyy(){
		simpleDateFormat = new SimpleDateFormat(date_format_dd_LLLL_yyyy, Locale.US);
		return simpleDateFormat.format(calendar.getTime());
	}

	// <used-in="Soap::Supervosr
	public String getCurrentddMMyy(){
		return getCurrentDate() + " " + getDescCurrentMonth(C.bahasa_indonesia, 0) + " " + getCurrentYear();
	}

	public String convertddMMyyyy(String value) {
		String rtn = "";
		try {
			simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
			Date temp = simpleDateFormat.parse(value);
			simpleDateFormat = new SimpleDateFormat(date_format_ddMMyyyy, Locale.US);
			rtn = simpleDateFormat.format(temp);
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String convertddMMMyyyy(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_ddMMMyyyy, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String convertddMMMyyyyToyyyyMMdd(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_ddMMMyyyy, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String convertddMMMyyyy(Date value) {
		String rtn = "";
		try {
			simpleDateFormat = new SimpleDateFormat(date_format_ddMMMyyyy, Locale.US);
			rtn = simpleDateFormat.format(value);
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String getCurrentDay(String language) {
		String rtn = "Monday";
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		switch(day) {
			case Calendar.SUNDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Minggu";
				} else {
					rtn = "Sunday";
				}
				break;
			case Calendar.MONDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Senin";
				} else {
					rtn = "Monday";
				}
				break;
			case Calendar.TUESDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Selasa";
				} else {
					rtn = "Tuesday";
				}
				break;
			case Calendar.WEDNESDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Rabu";
				} else {
					rtn = "Wednesday";
				}
				break;
			case Calendar.THURSDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Kamis";
				} else {
					rtn = "Thursday";
				}
				break;
			case Calendar.FRIDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Jumat";
				} else {
					rtn = "Friday";
				}
				break;
			case Calendar.SATURDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Sabtu";
				} else {
					rtn = "Saturday";
				}
				break;
		}
		return rtn;
	}

	public int getCurrentMonth() {
		return calendar.get(Calendar.MONTH) + 1;
	}

	public int getCurrentYear() {
		return calendar.get(Calendar.YEAR);
	}

	public int getCurrentDate() {
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	public int getDaysInMonth() {
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public String getDescCurrentMonth(String language, int param_month) {
		String rtn = "JAN";
		int month = calendar.get(Calendar.MONTH);
		if(param_month > 0) {
			month = param_month - 1;
		}
		switch(month) {
			case Calendar.JANUARY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Januari";
				} else {
					rtn = "Jan";
				}
				break;
			case Calendar.FEBRUARY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Februari";
				} else {
					rtn = "Feb";
				}
				break;
			case Calendar.MARCH:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Maret";
				} else {
					rtn = "Mar";
				}
				break;
			case Calendar.APRIL:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "April";
				} else {
					rtn = "Apr";
				}
				break;
			case Calendar.MAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Mei";
				} else {
					rtn = "May";
				}
				break;
			case Calendar.JUNE:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Juni";
				} else {
					rtn = "Jun";
				}
				break;
			case Calendar.JULY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "July";
				} else {
					rtn = "Jul";
				}
				break;
			case Calendar.AUGUST:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Agustus";
				} else {
					rtn = "Aug";
				}
				break;
			case Calendar.SEPTEMBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "September";
				} else {
					rtn = "Sep";
				}
				break;
			case Calendar.OCTOBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Oktober";
				} else {
					rtn = "Oct";
				}
				break;
			case Calendar.NOVEMBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "November";
				} else {
					rtn = "Nov";
				}
				break;
			case Calendar.DECEMBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "December";
				} else {
					rtn = "Dec";
				}
				break;
		}
		return rtn;
	}

	public String getDescPeriode(int periode, String language) {
		String rtn = "";

		try {
			String strPeriode = String.valueOf(periode);

			if (strPeriode.length() == 6){
				int month = Integer.parseInt(strPeriode.substring(4));
				month--;

				switch(month) {
					case Calendar.JANUARY:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "Januari";
						} else {
							rtn = "Jan";
						}
						break;
					case Calendar.FEBRUARY:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "Februari";
						} else {
							rtn = "Feb";
						}
						break;
					case Calendar.MARCH:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "Maret";
						} else {
							rtn = "Mar";
						}
						break;
					case Calendar.APRIL:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "April";
						} else {
							rtn = "Apr";
						}
						break;
					case Calendar.MAY:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "Mei";
						} else {
							rtn = "May";
						}
						break;
					case Calendar.JUNE:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "Juni";
						} else {
							rtn = "Jun";
						}
						break;
					case Calendar.JULY:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "July";
						} else {
							rtn = "Jul";
						}
						break;
					case Calendar.AUGUST:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "Agustus";
						} else {
							rtn = "Aug";
						}
						break;
					case Calendar.SEPTEMBER:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "September";
						} else {
							rtn = "Sep";
						}
						break;
					case Calendar.OCTOBER:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "Oktober";
						} else {
							rtn = "Oct";
						}
						break;
					case Calendar.NOVEMBER:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "November";
						} else {
							rtn = "Nov";
						}
						break;
					case Calendar.DECEMBER:
						if(language.equals(C.bahasa_indonesia)) {
							rtn = "December";
						} else {
							rtn = "Dec";
						}
						break;
				}

				rtn += " " + strPeriode.substring(0,4);
			}
		} catch (Exception ex){
			rtn = "";
			new UT_exception_handler().onExceptionHandler(ex);
		}

		return rtn;
	}

	public String getDescLastMonth(String language) {
		String rtn = "JAN";
		int day = calendar.get(Calendar.MONTH);

		day -= 1;

		switch(day) {
			case Calendar.JANUARY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Januari";
				} else {
					rtn = "Jan";
				}
				break;
			case Calendar.FEBRUARY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Februari";
				} else {
					rtn = "Feb";
				}
				break;
			case Calendar.MARCH:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Maret";
				} else {
					rtn = "Mar";
				}
				break;
			case Calendar.APRIL:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "April";
				} else {
					rtn = "Apr";
				}
				break;
			case Calendar.MAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Mei";
				} else {
					rtn = "May";
				}
				break;
			case Calendar.JUNE:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Juni";
				} else {
					rtn = "Jun";
				}
				break;
			case Calendar.JULY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "July";
				} else {
					rtn = "Jul";
				}
				break;
			case Calendar.AUGUST:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Agustus";
				} else {
					rtn = "Aug";
				}
				break;
			case Calendar.SEPTEMBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "September";
				} else {
					rtn = "Sep";
				}
				break;
			case Calendar.OCTOBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Oktober";
				} else {
					rtn = "Oct";
				}
				break;
			case Calendar.NOVEMBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "November";
				} else {
					rtn = "Nov";
				}
				break;
			case Calendar.DECEMBER:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "December";
				} else {
					rtn = "Dec";
				}
				break;
		}
		return rtn;
	}

	public String getTimeLog() {
		this.calendar = Calendar.getInstance();
		return getCurrentddMMyyyyHHmmss();
	}

	public String getGreet(String language) {
		String rtn = "Hello";
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		if(hour >= 18) {
			if(language.equals(C.bahasa_indonesia)) {
				rtn = "Selamat Malam";
			} else {
				rtn = "Evening";
			}
		} else if(hour >= 15) {
			if(language.equals(C.bahasa_indonesia)) {
				rtn = "Selamat Sore";
			} else {
				rtn = "Afternoon";
			}
		} else if(hour >= 11) {
			if(language.equals(C.bahasa_indonesia)) {
				rtn = "Selamat Siang";
			} else {
				rtn = "Afternoon";
			}
		} else {
			if(language.equals(C.bahasa_indonesia)) {
				rtn = "Selamat Pagi";
			} else {
				rtn = "Morning";
			}
		}

		return rtn;
	}

	public String convertyyyyMMtoMMMyyyy(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMM, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_MMMyyyy, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}



	public Date convertyyyyMMddHHmmss(String value) {
		Date rtn = new Date();
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd_HHmmsss, Locale.US);
				rtn = simpleDateFormat.parse(value);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public Date convertyyyyMMdd(String value) {
		Date rtn = new Date();
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				rtn = simpleDateFormat.parse(value);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

    public String convertyyyyMMdd2(String value) {
        String rtn = "";
        try {
            if(!value.equals("")) {
                simpleDateFormat = new SimpleDateFormat(date_format_ddMMyyyy, Locale.US);
                Date temp = simpleDateFormat.parse(value);
                simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
                rtn = simpleDateFormat.format(temp);

            }
        } catch(Exception e) {
            new UT_exception_handler().onExceptionHandler(e);
        }
        return rtn;
    }

	public String convertyyyyMMddtoyyMMdd(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_yyMMdd, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String convertyyyyMMddtoddMMMyyyy(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_ddMMMyyyy, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String convertyyyyMMddHHmmsstoyyMMddHHmm(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd_HHmmsss, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_yyMMddHHmm, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String parseyyyyMMdd(Date value) {
		String rtn = "";
		try {
			simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
			rtn = simpleDateFormat.format(value);
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String getPeriodMMYYYY(int period) {
		String rtn = "";
		try {
			String temp = String.valueOf(period);
			if(temp.length() == 6) {
				String year = temp.substring(0, 4);
				String mm = temp.substring(4, 6);
				String month = getDescCurrentMonth(C.bahasa_indonesia, Integer.valueOf(mm));
				rtn = month + " " + year;
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public int addPeriod(int period, int interval) {
		int rtn = 0;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(date_format_yyyyMM, Locale.US);
			Date curr = sdf.parse(String.valueOf(period));
			Calendar cal = Calendar.getInstance();
			cal.setTime(curr);
			cal.add(Calendar.MONTH, interval);
			rtn = Integer.valueOf(sdf.format(cal.getTime()));
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

    public String getCurrentyyMMddHHmmss(){
        simpleDateFormat = new SimpleDateFormat(date_format_yyMMdd_HHmmsss, Locale.US);

        return simpleDateFormat.format(calendar.getTime());
    }

    public String getPhotoDateTime(){
        simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd_HHmmsss_photo, Locale.US);

        return simpleDateFormat.format(calendar.getTime());
    }
	// <used-in="IceCream::Sales;"/>
	public String getCurrentddMMMyyyy(){
		simpleDateFormat = new SimpleDateFormat(date_format_ddMMMyyyy, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}
	// EndRegion 

	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor
	public String Y002_getDayNameFromDate(String value, String language) {
		String rtn = "Monday";

		try{
			simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
			Date temp = simpleDateFormat.parse(value);
			calendar.setTime(temp);
		} catch (Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}

		int day = calendar.get(Calendar.DAY_OF_WEEK);
		switch(day) {
			case Calendar.SUNDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Minggu";
				} else {
					rtn = "Sunday";
				}
				break;
			case Calendar.MONDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Senin";
				} else {
					rtn = "Monday";
				}
				break;
			case Calendar.TUESDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Selasa";
				} else {
					rtn = "Tuesday";
				}
				break;
			case Calendar.WEDNESDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Rabu";
				} else {
					rtn = "Wednesday";
				}
				break;
			case Calendar.THURSDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Kamis";
				} else {
					rtn = "Thursday";
				}
				break;
			case Calendar.FRIDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Jumat";
				} else {
					rtn = "Friday";
				}
				break;
			case Calendar.SATURDAY:
				if(language.equals(C.bahasa_indonesia)) {
					rtn = "Sabtu";
				} else {
					rtn = "Saturday";
				}
				break;
		}
		return rtn;
	}

	public int Y002_calculateTimeInMinutes(String time1, String time2){
		int rtn = 0;

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			Date date1 = sdf.parse(time1);
			Date date2 = sdf.parse(time2);

			long diff = date2.getTime() - date1.getTime();
			long diffSec = diff / 1000;
			long diffMinutes = diffSec / 60;

			if (diffMinutes <= 0)
				rtn = 0;
			else
				rtn = (int) diffMinutes;

		} catch (Exception ex){
			new UT_exception_handler().onExceptionHandler(ex);
		}

		return rtn;
	}

	public String Y002_convertMMMyyyy(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_MMMyyyy, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String Y002_convertyyyyMMddtoyyyyMM(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMM, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String Y002_getMM(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_MM, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String Y002_getYYYY(String value) {
		String rtn = "";
		try {
			if(!value.equals("")) {
				simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd, Locale.US);
				Date temp = simpleDateFormat.parse(value);
				simpleDateFormat = new SimpleDateFormat(date_format_yyyy, Locale.US);
				rtn = simpleDateFormat.format(temp);
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	public String Y002_getCurrentMMMyyyy(){
		simpleDateFormat = new SimpleDateFormat(date_format_MMMyyyy, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	public String Y002_convertMinutesToTime(int total_minutes){
		String rtn = "0h 0m";

		try {
			if (total_minutes > 0){
				int hours = total_minutes / 60;
				int minutes = total_minutes % 60;

				rtn = hours + "h " + minutes + "m ";
				//rtn = String.format("%02d:%02d",hours,minutes);
			}
		} catch (Exception ex){
			new UT_exception_handler().onExceptionHandler(ex);
		}

		return rtn;
	}

	public String Y002_getddMMyyyy(String strDate) {
		String rtn = "";
		try {
			if (strDate == null) strDate = "";

			if(strDate.length() == 10) {
				String year = strDate.substring(0, 4);
				String mm = strDate.substring(5, 7);
				String day = strDate.substring(8, 10);
				String month = getDescCurrentMonth(C.bahasa_indonesia, Integer.valueOf(mm));

				System.out.println("year : " + year);
				System.out.println("mm : " + mm);
				System.out.println("day : " + day);
				rtn = day + " " + month + " " + year;
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}

	// <used-in="Soap::Supervisor;IceCream::Sales;Soap::Sales"/>
	public String Y002_getPhotoDateTime(){
		simpleDateFormat = new SimpleDateFormat(date_format_yyyyMMdd_HHmmsss_photo, Locale.US);

		return simpleDateFormat.format(calendar.getTime());
	}

	// StartRegion +FSXX.17112016
	public String Y002_getDayBySeq(String value, String language) {
		String rtn = "-";

		try{
			if (value != null && !value.trim().equals("")){
				if (value.trim().equals("1")){
					if(language.equals(C.bahasa_indonesia)) {
						rtn = "Senin";
					} else {
						rtn = "Monday";
					}
				} else if (value.trim().equals("2")){
					if(language.equals(C.bahasa_indonesia)) {
						rtn = "Selasa";
					} else {
						rtn = "Tuesday";
					}
				} else if (value.trim().equals("3")){
					if(language.equals(C.bahasa_indonesia)) {
						rtn = "Rabu";
					} else {
						rtn = "Wednesday";
					}
				} else if (value.trim().equals("4")){
					if(language.equals(C.bahasa_indonesia)) {
						rtn = "Kamis";
					} else {
						rtn = "Thursday";
					}
				} else if (value.trim().equals("5")){
					if(language.equals(C.bahasa_indonesia)) {
						rtn = "Jumat";
					} else {
						rtn = "Friday";
					}
				} else if (value.trim().equals("6")){
					if(language.equals(C.bahasa_indonesia)) {
						rtn = "Sabtu";
					} else {
						rtn = "Saturday";
					}
				} else if (value.trim().equals("7")){
					if(language.equals(C.bahasa_indonesia)) {
						rtn = "Minggu";
					} else {
						rtn = "Sunday";
					}
				}
			}
		} catch (Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}

		return rtn;
	}

	public String getMonth3Name(String param) {
		String rtn = "NaM";
		switch(param) {
			case "01":
				rtn = "Jan";
				break;
			case "02":
				rtn = "Feb";
				break;
			case "03":
				rtn = "Mar";
				break;
			case "04":
				rtn = "Apr";
				break;
			case "05":
				rtn = "May";
				break;
			case "06":
				rtn = "Jun";
				break;
			case "07":
				rtn = "Jul";
				break;
			case "08":
				rtn = "Aug";
				break;
			case "09":
				rtn = "Sep";
				break;
			case "10":
				rtn = "Oct";
				break;
			case "11":
				rtn = "Nov";
				break;
			case "12":
				rtn = "Dec";
				break;
		}
		return rtn;
	}
	public int returnCalendarDay(String date){
		Date value = convertyyyyMMdd(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(value);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	public String returnTomorrow(String date){
		String rtn = "";
		Date value = convertyyyyMMdd(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(value);
		cal.add(Calendar.DATE,1);
		value = cal.getTime();
		return parseyyyyMMdd(value);
	}
	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
