package com.SFAWingsV2.libs.ut;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class UT_formatter {
	// StartRegion Global
public UT_formatter() {}
	
	public String formatNumber(String value) {
		String rtn = "";
		if(value == null || value.equals("")) {
			rtn = "0";
		} else {
			try{
			int intVal = Integer.valueOf(value);
			DecimalFormat formatter = new DecimalFormat("#,###,###");
			rtn = formatter.format(intVal);
			}catch(Exception e){
				try{
					double doubleVal = Double.parseDouble(value);	
					rtn = formatNumberWithDecimal(doubleVal);
				}catch(Exception ex){
					rtn = "NaN";
				}
			}
		
		}
		
		return rtn;
	}
	
	public String formatNumber(double value) {
		String rtn = "";
		DecimalFormat formatter = new DecimalFormat("#,###,###");
		rtn = formatter.format(value);
		
		return rtn;
	}
	
	public String formatNumberWithDecimal(double value) {
		String rtn = "";
		DecimalFormat formatter = new DecimalFormat("#,###,###.##");
		rtn = formatter.format(value);
		
		return rtn;
	}
	
	public String formatNumber(int value) {
		String rtn = "";
		DecimalFormat formatter = new DecimalFormat("#,###,###");
		rtn = formatter.format(value);
		
		return rtn;
	}
	
	public String toString(int value) { 
		return String.format(toString(value,4));
	}
	
	public String toString(String value) { 
		return String.format(toString(value,4));
	}
	
	public String toString(double value) { 
		return String.format(toString(value,4));
	}
	
	public String toString(double value, int numberOfSpaces) { 
		return String.format("%" + numberOfSpaces + "s", formatNumber(value));
	}
	
	public String toString(int value, int numberOfSpaces) { 
		return String.format("%" + numberOfSpaces + "s", formatNumber(value));
	}
	
	public String toString(String value, int numberOfSpaces) { 
		return String.format("%" + numberOfSpaces + "s", value);
	} 
	
	public String formatPercentage(String value) {
		String rtn = "";
		if(value == null || value.equals("")) {
			rtn = "0";
		} else {
			try {
				int intVal = Integer.valueOf(value);
				DecimalFormat formatter = new DecimalFormat("###.##");
				rtn = formatter.format(intVal);
			} catch (Exception e) {
				// TODO: handle exception
				try {
					double doubleVal = Double.parseDouble(value);	
					DecimalFormat formatter = new DecimalFormat("###.##");
					rtn = formatter.format(doubleVal);
				} catch (Exception e2) {
					// TODO: handle exception
					rtn = "NaN";
				}
			}
			
		}
		return rtn;
	}
	
	public String formatPercentage(double value) {
		String rtn = "";
		DecimalFormat formatter = new DecimalFormat("###.##");
		rtn = formatter.format(value);
		return rtn;
	}
	
	public String formatInformation(String value) {
		String rtn = "-";
		if(value != null && !value.trim().equals("")) {
			rtn = value;
		}
		return rtn;
	}
	
	/* {+FS6676 */
	public String formatShortened(double value) {
		String rtn = "";
		DecimalFormat formatter = new DecimalFormat("###,###.##");
		rtn = formatter.format(value);
		return rtn;
	}
	
	public String formatIndex(double value) {
		String rtn = "";
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');
		DecimalFormat formatter = new DecimalFormat("###.#", otherSymbols);
		rtn = formatter.format(value);
		return rtn;
	}
	
	public String formatIndex2(double value) {
		String rtn = "";
		DecimalFormat formatter = new DecimalFormat("###");
		rtn = formatter.format(value);
		return rtn;
	}
	
	public String formatHourMinutefromMinute(double value) {
		String rtn = "";
		int second = (int)Math.floor(value*60);
		
		int act_hour = (int)Math.floor(second/3600);
		second = second - act_hour*3600;
		int act_minute = (int)Math.floor(second/60);
		rtn = act_hour+"H "+act_minute+"M";
		
		return rtn;
	}
	/* } */
	
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
