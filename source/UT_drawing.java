/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */

package com.SFAWingsV2.libs.ut;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.ThumbnailUtils;
import android.view.MotionEvent;
import android.view.View;

import java.io.ByteArrayOutputStream;

@SuppressWarnings("unused")
@SuppressLint("ClickableViewAccessibility")
public class UT_drawing 
	extends View {
	
	// StartRegion Global
	public int width;
	public int height;
	private Bitmap mBitmap;
	private Canvas mCanvas;
	private Path mPath;
	private Paint mBitmapPaint;
	private Context context;
	private Paint circlePaint;
	private Path circlePath;
	private Paint mPaint;
	private boolean isDrawing = false;

	public UT_drawing(Context context, Paint paint) {
		super(context);
		
		this.context = context;
		this.mPaint = paint;
		
		mPath = new Path();
		mBitmapPaint = new Paint(Paint.DITHER_FLAG);
		circlePaint = new Paint();
		circlePath = new Path();
		circlePaint.setAntiAlias(true);
		circlePaint.setColor(Color.GRAY);
		circlePaint.setStyle(Paint.Style.STROKE);
		circlePaint.setStrokeJoin(Paint.Join.MITER);
		circlePaint.setStrokeWidth(2f);
		
		setDrawingCacheEnabled(true);
	}
	
	public boolean isDrawing() {
		return isDrawing;
	}

	public void setDrawing(boolean isDrawing) {
		this.isDrawing = isDrawing;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

	    width = w;     
	    height = h;

	    mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	    mCanvas = new Canvas(mBitmap);
	}
	
	public void clearDrawing(){
		isDrawing = false;
		
	    setDrawingCacheEnabled(false);
	    
	    onSizeChanged(width, height, width, height);
	    invalidate();

	    setDrawingCacheEnabled(true);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

		canvas.drawPath(mPath, mPaint);

		canvas.drawPath(circlePath, circlePaint);
	}

	private float mX, mY;
	private static final float TOUCH_TOLERANCE = 4;

	private void touch_start(float x, float y) {
		mPath.reset();
		mPath.moveTo(x, y);
		mX = x;
		mY = y;
	}

	private void touch_move(float x, float y) {
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
			mX = x;
			mY = y;

			circlePath.reset();
			circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
		}
	}

	private void touch_up() {
		mPath.lineTo(mX, mY);
		circlePath.reset();
		mCanvas.drawPath(mPath, mPaint);
		mPath.reset();
	}
	
	public byte[] saveDrawing(){
	  Bitmap bitmap = getDrawingCache();

	  bitmap =
	         ThumbnailUtils.extractThumbnail(bitmap, 256, 256);

	  ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	  bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
	  return outputStream.toByteArray();
	}
	
	public Bitmap getBitmap(){
	  return getDrawingCache();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();
		
		isDrawing = true;
		
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touch_start(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_MOVE:
			touch_move(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_UP:
			touch_up();
			invalidate();
			break;
		}
		return true;
	}
	// EndRegion 
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
