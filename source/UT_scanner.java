package com.SFAWingsV2.libs.ut;

import android.content.Intent;
import android.os.Bundle;

import com.SFAWingsV2.libs.rs.C;
import com.SFAWingsV2.ui.Activity_Base;

public class UT_scanner extends Activity_Base {
	// StartRegion Global
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_pre_collection_list);
		initActivity();
		initUI();
	}
	
	@Override
	protected void initActivity() {
		super.initActivity();
	}
	
	@Override
	protected void initUI() {
		super.initUI();
		
		Intent intent = getIntent();
		int mode = intent.getIntExtra("scan_mode", -1);
    	Intent scan = new Intent("com.google.zxing.client.android.SCAN");
    	scan.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    	scan.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(scan, mode);
	        
	}
	
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		Intent result = new Intent();
    	if(resultCode == RESULT_OK) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            
    		switch(requestCode) {
	    		case C.scan_customer:
	    			if(contents.startsWith("C")) {
	    				String[] temp = contents.split(" ");
	    				
	    				result.putExtra("scan_result", temp[1]);
	    				setResult(RESULT_OK, result);
	    				
	    			} else {
	    				result.putExtra("scan_result", "");
	    				setResult(RESULT_OK, result);
	    			}
	    			break;
	    			
	    		case C.scan_office:
	    			if(contents.startsWith("O")) {
	    				String[] temp = contents.split(" ");
	    				
	    				result.putExtra("scan_result", temp[1]);
	    				setResult(RESULT_OK, result);
	    				
	    			} else {
	    				result.putExtra("scan_result", "");
	    				setResult(RESULT_OK, result);
	    			}
	    			break;
	    			
	    		case C.register:
	    			if(contents.startsWith("C")) {
	    				String[] temp = contents.split(" ");
	    				
	    				result.putExtra("scan_result", temp[1]);
	    				setResult(RESULT_OK, result);
	    				
	    			} else {
	    				result.putExtra("scan_result", "");
	    				setResult(RESULT_OK, result);
	    			}
	    			break;
    		}
    		
    	} else if(resultCode == RESULT_CANCELED) {
			setResult(RESULT_CANCELED, result);
    	}

    	finish();
    }
	// EndRegion 
    
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
