/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */

package com.SFAWingsV2.libs.ut;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import com.SFAWingsV2.libs.rs.C;

/**
 * Utility Basic Class
 * 
 * @author
 * 	Yohanes Agung Immanuel
 * @since
 * 	March 2016
 * @see
 * 	1. All class on <b>com.SFAWingsV2.libs.ut</b> has been updated with region to support code folding <br>
 *  2. To add region, please follow this template bellow : <br>
 *  	<pre>
 *  	{@code
 *  	// StartRegion Module::SubModule
 *  	
 *   	// EndRegion
 *   	}</pre>
 *     Where Module like a Business Area (eg. Soap, IceCream, and Etc.)<br>
 *     Where SubModule like Intended and associated for use and with the user (eg. Sales, Supervisor, Merchandising, VanSales, Etc.)<br><br>
 *     
 *     Please check this example bellow :<br>
 *     <pre>
 *   	{@code
 *  	// StartRegion Soap::Sales
 *  	
 *   	// EndRegion
 *     	}</pre>
 *     
 *     Rules of Thumb both Module and SubModule<br>
 *     1. All in CammelCase. (Eg. (IceCream)<br>
 *     2. No Space Allowed if Module or SubModule consists of two words. (Eg. VanSales)<br>
 *  3. For multipurpose function that used from outside region (function that used more than one module)
 *  	please give a tag like bellow :<br>
 *  	<pre>
 *  	{@code
 *  	// <used-in="Module1::SubModule1;Module2::SubModule2;((...));ModuleN::SubModuleN"/>
 *  	}</pre>
 *  
 *  	Please check example bellow :  <br>
 *  	<pre>
 *  	{@code
 *  	// StartRegion Soap::Sales
 *  	((some code here))
 *    	
 *  	// <used-in="Soap::Supervisor;IceCream::Sales;IceCream::Supervisor"/>
 *  	public BL_m_cmb_schedule_bcp(Context context){
 *  		((some code here))
 *   	}
 *
 *   	((some code here))
 *     	// EndRegion
 *		</pre>
 *  4. Please follow the rules to make consistency and well-orginized library<br>
 *  5. Thanks for your cooperation and understanding<br>
 */
public class UT_base {
	private Context context;
	
	public UT_base(){
		this.context = null;
	}
	
	public UT_base(Context context){
		this.context = context;
	}
	
	// StartRegion Global
	public void exitApplication(){
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(1);
	}
	
	public String getProjectModule(){
		if(context != null)
			return getMetaData(C.mod).toString();
		else
			return null;
	}
	
	private Object getMetaData(String name) {		
		if(context != null){
			try {
		        ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
		        Object value = (Object)applicationInfo.metaData.get(C.mod);
		        
		        return value;
		    } catch (NameNotFoundException e) {
		        e.printStackTrace();
		    }
		}
		
		return null;
	}
	// EndRegion
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
