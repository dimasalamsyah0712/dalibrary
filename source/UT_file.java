package com.SFAWingsV2.libs.ut;

import android.os.Environment;
import android.util.Base64;

import com.SFAWingsV2.libs.rs.C;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class UT_file {
	public String file_path = "";
	
	public UT_file(String sub_folder, boolean delete) {
		this.file_path = Environment.getExternalStorageDirectory() + C.Y002_file_folder + sub_folder;
		if(!delete) {
			checkMainFolder();
			checkSubFolder();
		}
	}
	
	public UT_file(){}
	
	public void checkMainFolder() {
		File folder = new File(Environment.getExternalStorageDirectory() + C.Y002_file_folder);
		if(!folder.exists()) {
			folder.mkdir();
		}
	}
	
	public void checkSubFolder() {
		File folder = new File(file_path);
		if(!folder.exists()) {
			folder.mkdir();
		}
	}
		
	public boolean deleteAllFiles() {
		boolean rtn = false;
		try {
			File folder = new File(file_path);
			if(folder.exists() && folder.isDirectory()) {
				String[] files = folder.list();
				int size = files.length;
				for(int i = 0; i < size; i++) {
					File file = new File(folder, files[i]);
					if(file.isFile()) {
						file.delete();
					}
				}
				
				rtn = folder.delete();
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}
	
	public boolean deleteFile(String file_name) {
		boolean rtn = false;
		try {
			File file = new File(file_path, file_name);
			if(file.exists() && file.isFile()) {
				rtn = file.delete();
			}
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}	
	
	public int storeFile(String file_byte, String file_name) {
		int rtn = 0;
		try {
			File imageFile = new File(file_path, file_name);
			
			OutputStream outStream = new FileOutputStream(imageFile);
			outStream.write(Base64.decode(file_byte, 0));
			outStream.flush();
            outStream.close();
			
			rtn = 1;
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		return rtn;
	}	
	
	public File getFile(String path){
		File rtn = null;
		
		try{
			String pathName = file_path + File.separator + path;
			rtn = new File(pathName);
		}catch(Exception e){
			new UT_exception_handler().onExceptionHandler(e);
		}
		
		return rtn;
	}	
}
