package com.SFAWingsV2.libs.ut;

import android.widget.Spinner;

public class UT_helper {
	public static int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
}
}
