package com.SFAWingsV2.libs.ut;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.SFAWingsV2.R;
import com.SFAWingsV2.libs.be.BE_Map_Pin;
import com.SFAWingsV2.libs.be.BE_Map_Point;
import com.SFAWingsV2.libs.be.BE_m_system;
import com.SFAWingsV2.libs.bl.BL_m_customer_location;
import com.SFAWingsV2.libs.rs.C;
import com.SFAWingsV2.libs.sp.SP_m_system;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import android.widget.Spinner;

public class UT_map extends SurfaceView implements SurfaceHolder.Callback{
	// StartRegion Global
	protected Context context;	
	protected Canvas canvas;
	private UT_surface_thread mythread;
	private Boolean isPressed = false;
	private Boolean isZoom = false;
	private float oriFingerDistance = 0;
	private Bitmap pinWhite;
	private Bitmap pinYellow;
	private Bitmap pinGreen;
	private Bitmap pinSales;
	private Map<String, Bitmap> dictMap;
	private ArrayList<BE_Map_Pin> lstPin;
	private BE_Map_Pin salesPin;
	private float TILE_SIZE = 256;
	private int zoom = 15;
	private BE_Map_Point centerPixel = null;
	private BE_Map_Point topLeft = null;
	private BE_Map_Point lastPos = new BE_Map_Point();
	private Boolean allowDraw = true;
	private BE_Map_Pin focusedPin = null;
		
	public UT_map(Context ctx, AttributeSet attrSet){
		super(ctx, attrSet);
		
		context = ctx;
				
		pinWhite = BitmapFactory.decodeResource(getResources(), R.drawable.pinwhite);
		pinYellow = BitmapFactory.decodeResource(getResources(), R.drawable.pinyellow);
		pinGreen = BitmapFactory.decodeResource(getResources(), R.drawable.pingreen);
		pinSales = BitmapFactory.decodeResource(getResources(), R.drawable.pinsales);
		
		dictMap = new HashMap<String, Bitmap>();
		salesPin = new BE_Map_Pin();
		//centerPixel = new BE_Map_Point();
		
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		
		BE_m_system sales = new SP_m_system(context).read();
		loadCustomerPin(sales.sls_type);				
	}
	
	public void loadCustomerPin(String sls_type){
		lstPin = new ArrayList<BE_Map_Pin>();
		
		try {
			BL_m_customer_location bl_loc = new BL_m_customer_location(context);
			ArrayList<BE_Map_Pin> tempPin = new ArrayList<BE_Map_Pin>();
			
			if (sls_type.equalsIgnoreCase(C.Y002_to_spv)){
				tempPin = bl_loc.Y002_getCustomerPin();
			} else {
				tempPin = bl_loc.getCustomerPin();
			}
			
			int size = tempPin.size();
			for(int i = 0; i < size; i++) {
				BE_Map_Pin rec = new BE_Map_Pin();
				rec = tempPin.get(i);
				rec.pos = fromLatLngToPixel(rec.latitude, rec.longitude, zoom);
				lstPin.add(rec);
				
	    		if(centerPixel == null && rec.latitude != 0 && rec.longitude != 0){
					centerPixel = new BE_Map_Point();
	    			centerPixel.x = rec.pos.x;
	    			centerPixel.y = rec.pos.y;
    			}
			}
			
		} catch(Exception e) {
			new UT_exception_handler().onExceptionHandler(e);
		}
		
	}
	
	public BE_Map_Point fromLatLngToPixel(float lat, float lng, int zoom){        
		float x = (lng + 180) / 360;
        float sinLatitude = (float) Math.sin(lat * Math.PI / 180);
        float y = (float) (0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI));

        float mapSize = (float) (Math.pow(2, zoom) * TILE_SIZE);
       
        BE_Map_Point pt = new BE_Map_Point();
        pt.x = (float) (x * mapSize + 0.5);
        pt.y = (float) (y * mapSize + 0.5);
        return pt;
    }
	
	public BE_Map_Point fromPixelToLatLng(float x, float y, int zoom){        		
        float mapSize = (float) (Math.pow(2, zoom) * TILE_SIZE);
               
        float xx = (float) (x / mapSize - 0.5);
        float yy = (float) (0.5 - y / mapSize);
        
        BE_Map_Point pt = new BE_Map_Point();
        pt.x = (float) (90 - 360 * Math.atan(Math.exp(-yy * 2 * Math.PI)) / Math.PI);
        pt.y = 360 * xx;
        		               
        return pt;
    }
	
	public BE_Map_Point fromPixelToTileXY(BE_Map_Point pt)
    {
		BE_Map_Point tile = new BE_Map_Point();
		tile.x = (int)(pt.x / TILE_SIZE);
		tile.y = (int)(pt.y / TILE_SIZE);
        
		return tile;
    }
		 	 
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){

	}
	  
	@Override
	public void surfaceCreated(SurfaceHolder holder){
		mythread = new UT_surface_thread(holder, context, this);
		mythread.setRunning(true);
		mythread.start();
	}
	 
	@Override
	public void surfaceDestroyed(SurfaceHolder holder){		
		mythread.setRunning(false);
		boolean retry = true;
		while(retry){
			try{
				mythread.join();
				retry = false;
			}
			catch(Exception e){		 
				//Do Nothing
			}
		}
	}
	
	void doDraw(Canvas canvas){
		if(!allowDraw) return;
		
		Paint paintClear = new Paint();
		paintClear.setColor(Color.BLACK);
		paintClear.setStyle(Style.FILL);
		
		Paint paintWhite = new Paint();
		paintWhite.setColor(Color.WHITE);
						
		//Calculate Top Left
		topLeft = new BE_Map_Point();
		
		if(centerPixel == null) {
			centerPixel = salesPin.pos;
		}
		topLeft.x = centerPixel.x - canvas.getWidth() / 2;
		topLeft.y = centerPixel.y - canvas.getHeight() / 2;

		BE_Map_Point topLeftTile = fromPixelToTileXY(topLeft);
		BE_Map_Point offset = new BE_Map_Point();
		offset.x = topLeft.x % TILE_SIZE;
		offset.y = topLeft.y % TILE_SIZE;
				
		//Loading map tile files
		ArrayList<String> lstUsedImageFiles = new ArrayList<String>();
		ArrayList<String> lstNotUsedImageFiles = new ArrayList<String>();
		for(int x = (int) topLeftTile.x; x<= topLeftTile.x + 4; x++){
        	for(int y = (int) topLeftTile.y; y<= topLeftTile.y + 5; y++){
        		String filename = Environment.getExternalStoragePublicDirectory("sfa") + "/" + C.image_folder_maps + "/map" + zoom + "_" + x + "_" + y + ".png";
        		lstUsedImageFiles.add(filename);
        		
        		if(!dictMap.containsKey(filename)){
        			try{   
        				File file = new File(filename);
        			    if(file.exists()){
        			    	Bitmap bmp = BitmapFactory.decodeFile(filename);
        			    	if(bmp != null) dictMap.put(filename, bmp);        				
        			    }
        			}
        			catch(Exception ex){
        				if(ex != null) Log.e("Wings", ex.getMessage());
        			}
        		}        		
        	}
        }
		
		//Remove undisplayed image files from memory
		for(String file: dictMap.keySet()){
			if(!lstUsedImageFiles.contains(file)) lstNotUsedImageFiles.add(file);
		}
		
		for(String file: lstNotUsedImageFiles){
			dictMap.get(file).recycle();
			dictMap.remove(file);
		}

		//Draw the map tiles		
        for(int x = (int) topLeftTile.x; x<= topLeftTile.x + 4; x++){
        	for(int y = (int) topLeftTile.y; y<= topLeftTile.y + 5; y++){
        		String filename = Environment.getExternalStoragePublicDirectory("sfa") + "/" + C.image_folder_maps + "/map" + zoom + "_" + x + "_" + y + ".png";
        		float pixelX = 0 - offset.x + TILE_SIZE * (x - topLeftTile.x);
        		float pixelY = 0 - offset.y + TILE_SIZE * (y - topLeftTile.y);
        		
        		if(dictMap.containsKey(filename)){
        			canvas.drawBitmap(dictMap.get(filename), pixelX, pixelY, null);
        		}
        		else{
        			canvas.drawRect(pixelX, pixelY, pixelX + TILE_SIZE, pixelY + TILE_SIZE, paintClear);        			
        		}
        	}
        }
        
        //Draw Customer Pin
        for(BE_Map_Pin pin: lstPin){
        	if(focusedPin!= null && !focusedPin.namaCustomer.equals("ALL") && !focusedPin.kodeCustomer.equals(pin.kodeCustomer)) continue;
        	
        	Bitmap bmp = pinWhite;
        	if(pin.isOrdering){
            	bmp = pinGreen;
            }
            else if(pin.isVisited){
            	bmp = pinYellow;
            }
        	
        	if(bmp != null) canvas.drawBitmap(bmp, pin.pos.x - topLeft.x - bmp.getWidth()/2, pin.pos.y - topLeft.y - bmp.getHeight(), null);
        }
        
        //Draw Sales Pin
        salesPin.pos = fromLatLngToPixel(salesPin.latitude, salesPin.longitude, zoom);
        if(salesPin.latitude != 0 && salesPin.longitude != 0){ 
        	canvas.drawBitmap(pinSales, salesPin.pos.x - topLeft.x - pinSales.getWidth()/2, salesPin.pos.y - topLeft.y - pinSales.getHeight(), null);
        	if(focusedPin!= null && !focusedPin.namaCustomer.equals("ALL")){
        		Paint paintLine = new Paint();
        		paintLine.setColor(Color.MAGENTA);
        		paintLine.setAntiAlias(true);
        		paintLine.setStrokeWidth(2);
        		canvas.drawLine(salesPin.pos.x - topLeft.x, salesPin.pos.y - topLeft.y, focusedPin.pos.x - topLeft.x, focusedPin.pos.y - topLeft.y, paintLine);
        	}
        }                
        
	}
	
	@SuppressLint("ClickableViewAccessibility")
	@SuppressWarnings("deprecation")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction();
		if (action == MotionEvent.ACTION_MOVE){
			float x =  event.getX();
			float y =  event.getY();
			if(isPressed && !isZoom){
				centerPixel.x -= x - lastPos.x;
				centerPixel.y -= y - lastPos.y;
				lastPos.x = x;
				lastPos.y = y;
			}
			
		} else if (action == MotionEvent.ACTION_DOWN){
			lastPos.x =  event.getX();
			lastPos.y =  event.getY();
			isPressed = true;
			checkTap(lastPos.x, lastPos.y);
			
		} else if(action == MotionEvent.ACTION_POINTER_2_DOWN){
			isZoom = true;
			oriFingerDistance = (float) Math.sqrt(Math.pow(lastPos.x - event.getX(1), 2) + Math.pow(lastPos.y - event.getY(1), 2));
			
		} else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP || action == MotionEvent.ACTION_POINTER_2_UP){
			try{
				isPressed = false;
				if(isZoom){
					isZoom = false;
					float newFingerDistance = (float) Math.sqrt(Math.pow(lastPos.x - event.getX(1), 2) + Math.pow(lastPos.y - event.getY(1), 2));
			
					if(newFingerDistance < oriFingerDistance){
						if(zoom == 17){ 
							changeZoom(15);
						}else if(zoom == 15){
							changeZoom(13);
						}
					}
					else{
						if(zoom == 13){ 
							changeZoom(15);
						}else if(zoom == 15){
							changeZoom(17);
						}
					}
				}
			}catch(Exception ex){
				Log.e("Wings", ex.getMessage());
			}
		}				
		
		return true;
	}
	
	public void checkTap(float x, float y){
		for(BE_Map_Pin pin: lstPin){
			if(focusedPin!= null && !focusedPin.namaCustomer.equals("ALL") && !focusedPin.kodeCustomer.equals(pin.kodeCustomer)) continue;
			
			if(x >= pin.pos.x - topLeft.x - pinWhite.getWidth()/2 && x <= pin.pos.x - topLeft.x + pinWhite.getWidth()/2 && 
	           y >= pin.pos.y - topLeft.y - pinWhite.getHeight() && y <= pin.pos.y - topLeft.y){			
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage(pin.alamatCustomer + "\n" + pin.kotaCustomer)
					.setTitle(pin.namaCustomer + " (" + pin.kodeCustomer + ")")
					.setIcon(android.R.drawable.ic_dialog_info);
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		}		
	}
	
	public void changeZoom(int newZoom){
		allowDraw = false;
		BE_Map_Point latLng = fromPixelToLatLng(centerPixel.x, centerPixel.y, zoom);
		zoom = newZoom;
		
		//Update centerPixel
		centerPixel = fromLatLngToPixel(latLng.x, latLng.y, zoom); 
		
		//Update pin pos
		for(BE_Map_Pin pin: lstPin){
			pin.pos = fromLatLngToPixel(pin.latitude, pin.longitude, zoom);
		}
		if(focusedPin != null) focusedPin.pos = fromLatLngToPixel(focusedPin.latitude, focusedPin.longitude, zoom);
		if(salesPin != null) salesPin.pos = fromLatLngToPixel(salesPin.latitude, salesPin.longitude, zoom);
		allowDraw = true;
		
//		Activity_Map activity = (Activity_Map) this.getContext();
//		Spinner cmbZoom = (Spinner) activity.findViewById(R.id.ActivityVisMap_spZoom);
//		if(zoom == 13) cmbZoom.setSelection(0);
//		if(zoom == 15) cmbZoom.setSelection(1);
//		if(zoom == 17) cmbZoom.setSelection(2);
	}
	
//	public void moveToPin(BE_Map_Pin pin){
//		if(pin.namaCustomer != "ALL" && pin.latitude == 0){
//			//No Lat Lng for this customer pin
//			AlertDialog.Builder builder = new AlertDialog.Builder(context);
//			builder.setMessage("Lokasi " + pin.namaCustomer + " belum ada!")
//				.setTitle("Error")
//				.setIcon(android.R.drawable.ic_dialog_alert);
//			AlertDialog dialog = builder.create();
//			dialog.show();
//			return;
//		}
//		
//		focusedPin = pin;
//		focusedPin.pos = fromLatLngToPixel(focusedPin.latitude, focusedPin.longitude, zoom);
//		if(pin.namaCustomer == "ALL"){
//			//ALL -> center on first customer with lat lng
//			for(int i=0;i<lstPin.size();i++){
//				if(lstPin.get(i).latitude != 0){
//					centerPixel = fromLatLngToPixel(lstPin.get(i).latitude, lstPin.get(i).longitude, zoom);
//					break;
//				}
//			}
//		}
//		else{
//			centerPixel = fromLatLngToPixel(focusedPin.latitude, focusedPin.longitude, zoom);
//		}
//	}
	
	public void changeSalesLocation(float lat, float lng){
		salesPin.latitude = lat;
		salesPin.longitude = lng;
		salesPin.pos = fromLatLngToPixel(salesPin.latitude, salesPin.longitude, zoom);
		
		if(lat == 0 && lng == 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage("Gagal menentukan posisi anda!")
				.setTitle("Posisi Anda")
				.setIcon(android.R.drawable.ic_dialog_map);
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
	// EndRegion
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
