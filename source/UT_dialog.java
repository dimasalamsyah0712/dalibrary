/*
 * @authors
 * 	Yohanes Agung Immanuel
 * @since
 * 	September 2015
 */


package com.SFAWingsV2.libs.ut;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.SFAWingsV2.R;
import com.SFAWingsV2.libs.be.BE_ui_two_col;
import com.SFAWingsV2.libs.rs.C;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("InflateParams")
public class UT_dialog {
	// StartRegion Global
	private Context context;
	private AlertDialog alertDialog;
	private AlertDialog.Builder builder;
	public Object result;
	
	// Getter and Setter :
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public AlertDialog getAlertDialog() {
		return alertDialog;
	}

	public void setAlertDialog(AlertDialog alertDialog) {
		this.alertDialog = alertDialog;
	}

	public AlertDialog.Builder getBuilder() {
		return builder;
	}

	public void setBuilder(AlertDialog.Builder builder) {
		this.builder = builder;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
	// Constructor :
	public UT_dialog(Context context){
		this.context = context;
		builder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);	
		alertDialog = builder.create();
		
		result = "";
	}	
	
	// Creator :
	public void showAlertOKBox(String pTittle, String pMessage){
		builder.setMessage(pMessage).setTitle(pTittle);
		builder.setPositiveButton(android.R.string.ok, 
			new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int id){
					dialog.dismiss();
				}
		});
		
		alertDialog = builder.create();
		alertDialog.show();
	}	
	
	public UT_dialog.DialogInfoOneButton showWarningBox(Activity activity, String title, String message, String strPositive){
		final UT_dialog.DialogInfoOneButton dialogInfoOneButton = new DialogInfoOneButton(
			activity,
			title,
			message,
			strPositive	
		);
		
		dialogInfoOneButton.setPositiveButtonListener(
			new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialogInfoOneButton.dismiss();
				}
			});
		
		
		return dialogInfoOneButton;
    }
	
	public UT_dialog.DialogInfoTwoButtons showWarningBox(String title, String message, 
			String strNegative, String strPositive, 
			View.OnClickListener oclNegative, View.OnClickListener oclPositive){
		
		UT_dialog.DialogInfoTwoButtons dialogInfoTwoButtons = new DialogInfoTwoButtons(
				(Activity) context,
				title,
				message,
				strNegative,
				strPositive,
				oclNegative,
				oclPositive
			);
			
		return dialogInfoTwoButtons;
	}
	
	public UT_dialog.DialogHtmlInfoOneButton showHtmlBoxOneButton(Activity activity, String title, String message, String strPositive, int boxMode){
		final UT_dialog.DialogHtmlInfoOneButton dialogHtmlInfoOneButton = new DialogHtmlInfoOneButton(
			activity,
			title,
			message,
			strPositive,
			boxMode
		);
		
		dialogHtmlInfoOneButton.setPositiveButtonListener(
			new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialogHtmlInfoOneButton.dismiss();
				}
			});
		
		
		return dialogHtmlInfoOneButton;
    }
	
	public UT_dialog.DialogHtmlInfoOneButton showHtmlOkBoxTwoCol(Activity activity, String title, BE_ui_two_col[] be_ui_two_cols, String strPositive, int boxMode){
		String message = "<html><head></head><body style='background-color:#d0d0d0'>";
		
		message += "<table>";
		for(BE_ui_two_col be_ui_two_col : be_ui_two_cols){
			message += "<tr>";
			message += "<td width='30%' valign='top'>" + be_ui_two_col.col1 + "</td><td valign='top' width='2%'> : </td><td valign='top' width='100%'>" + be_ui_two_col.col2 + "</td>";
			message += "</tr>";
		}		
		message += "</table>";
		
		message += "</body></html>";
		
		final UT_dialog.DialogHtmlInfoOneButton dialogHtmlInfoOneButton = new DialogHtmlInfoOneButton(
			activity,
			title,
			message,
			strPositive,
			boxMode
		);
		
		dialogHtmlInfoOneButton.setPositiveButtonListener(
			new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialogHtmlInfoOneButton.dismiss();
				}
			});
		
		
		return dialogHtmlInfoOneButton;
    }
		
	// Maker :
	public AlertDialog.Builder getDarkAlertDialogBuilder(){
		return new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);
	}
	
	public ProgressDialog getDarkProgressDialog(){
		return new ProgressDialog(context, ProgressDialog.THEME_HOLO_DARK);
	}

    public DatePickerDialog getDarkDatePickerDialog(DatePickerDialog.OnDateSetListener onDateSetListener, Calendar calendar){
        return new DatePickerDialog(
                context,
                android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth,
                onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    public DatePickerDialog getDarkDatePickerDialogWithMaxDate(DatePickerDialog.OnDateSetListener onDateSetListener, Calendar calendar, String maxdate){

        DatePickerDialog dpd =new DatePickerDialog(
                context,
                android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth,
                onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        Date rtn = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            rtn = simpleDateFormat.parse(maxdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dpd.getDatePicker().setMaxDate(rtn.getTime());

		return dpd;
	} 
	
	public DialogInfoOneButton getDialogInfoOneButton(Activity activity, String title, String message, String textButton1){
		return new DialogInfoOneButton(activity, title, message, textButton1);
	}
	
	public DialogInfoOneButton getDialogInfoOneButton(Activity activity, String title, String message, String textButton1, View.OnClickListener onClickListenerButton1){
		return new DialogInfoOneButton(activity, title, message, textButton1, onClickListenerButton1);
	}
	
	// Inner Class :
	public class DialogInfoTwoButtons{
		private LayoutInflater layoutInflater;
		private Activity activity;
		
		private View view;
		private TextView tvMessage, tvHeader;
		private Button btButton1, btButton2;
		
		private String strTitle, strMessage, strButton1, strButton2;
		private OnClickListener oclButton1, oclButton2;		
		
		public DialogInfoTwoButtons(Activity activity){
			this.activity = activity;
		}
		
		public DialogInfoTwoButtons(Activity activity, 
				String title, String message,
				String textButton1, String textButton2,
				View.OnClickListener onClickListenerButton1,
				View.OnClickListener onClickListenerButton2){
			this.activity = activity; 
			
			this.strTitle = title;
			this.strMessage = message;
			
			this.strButton1 = textButton1;
			this.strButton2 = textButton2;
			
			this.oclButton1 = onClickListenerButton1;
			this.oclButton2 = onClickListenerButton2;
		}
		
		public AlertDialog getDialogInfoTwoButtons(){
			layoutInflater = activity.getLayoutInflater();
			
			view = layoutInflater.inflate(R.layout.dialog_info_two_buttons_red_themes, null);
			
			initDialog();
			initUI();
			
			builder.setView(view);		
			alertDialog = builder.create();
					
			return alertDialog;
		}
		
		public DialogInfoTwoButtons setActivity(Activity activity) {
			this.activity = activity;			
			return this;
		}

		public DialogInfoTwoButtons setStrTitle(String strTitle) {
			this.strTitle = strTitle;
			return this;
		}

		public DialogInfoTwoButtons setStrMessage(String strMessage) {
			this.strMessage = strMessage;
			return this;
		}

		public DialogInfoTwoButtons setStrButton1(String strButton1) {
			this.strButton1 = strButton1;
			return this;
		}

		public DialogInfoTwoButtons setStrButton2(String strButton2) {
			this.strButton2 = strButton2;
			return this;
		}

		public DialogInfoTwoButtons setOclButton1(OnClickListener oclButton1) {
			this.oclButton1 = oclButton1;
			return this;
		}

		public DialogInfoTwoButtons setOclButton2(OnClickListener oclButton2) {
			this.oclButton2 = oclButton2;
			return this;
		}

		public void show(){
			alertDialog = getDialogInfoTwoButtons();
			alertDialog.show();
		}
		
		public void dismiss(){
			alertDialog.dismiss();
		}
		
		public void initDialog(){
			tvMessage = (TextView) view.findViewById(R.id.DialogInfoTwoButtonsRedThemes_tvMessage);
			tvHeader = (TextView) view.findViewById(R.id.DialogInfoTwoButtonsRedThemes_tvHeader);
			
			btButton1 = (Button) view.findViewById(R.id.DialogInfoTwoButtonsRedThemes_btButton1);
			btButton2 = (Button) view.findViewById(R.id.DialogInfoTwoButtonsRedThemes_btButton2);
		}
		
		public void initUI(){
			tvHeader.setText(Html.fromHtml(strTitle));
			tvMessage.setText(Html.fromHtml(strMessage));
			
			btButton1.setText(Html.fromHtml(strButton1));
			btButton2.setText(Html.fromHtml(strButton2));
			
			btButton1.setOnClickListener(oclButton1);
			btButton2.setOnClickListener(oclButton2);
		}	
	}
	
	public class DialogInfoOneButton{
		private LayoutInflater layoutInflater;
		private Activity activity;
		
		private View view;
		private TextView tvMessage, tvHeader;
		private Button btButton1;
		
		private String strTitle, strMessage, strButton1;
		private OnClickListener oclButton1;		
		
		public DialogInfoOneButton(Activity activity, 
				String title, String message,
				String textButton1,
				View.OnClickListener onClickListenerButton1){
			this.activity = activity;
			
			this.strTitle = title;
			this.strMessage = message;
			
			this.strButton1 = textButton1;
			
			this.oclButton1 = onClickListenerButton1;
		}
		
		public DialogInfoOneButton(Activity activity){
			this.activity = activity;
		}
		
		public DialogInfoOneButton(Activity activity, 
				String title, String message,
				String textButton1){
			this.activity = activity;
			
			this.strTitle = title;
			this.strMessage = message;
			
			this.strButton1 = textButton1;
			
			this.oclButton1 = new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					alertDialog.dismiss();
				}
			};
		}
		
		public AlertDialog getDialogInfoOneButtons(){
			layoutInflater = activity.getLayoutInflater();
			
			view = layoutInflater.inflate(R.layout.dialog_info_one_button_red_themes, null);
			
			initDialog();
			initUI();
			
			builder.setView(view);		
			alertDialog = builder.create();
					
			return alertDialog;
		}
				
		public DialogInfoOneButton setActivity(Activity activity) {
			this.activity = activity;
			return this;
		}

		public DialogInfoOneButton setPositiveButtonListener(OnClickListener oclButton1) {
			this.oclButton1 = oclButton1;
			return this;
		}

		public DialogInfoOneButton setStrTitle(String strTitle) {
			this.strTitle = strTitle;
			return this;
		}

		public DialogInfoOneButton setStrMessage(String strMessage) {
			this.strMessage = strMessage;
			return this;
		}

		public DialogInfoOneButton setStrButton1(String strButton1) {
			this.strButton1 = strButton1;
			return this;
		}

		public void show(){
			alertDialog = getDialogInfoOneButtons();
			alertDialog.show();
		}
				
		public void dismiss(){
			alertDialog.dismiss();
		}
		
		public void initDialog(){
			tvMessage = (TextView) view.findViewById(R.id.DialogInfoOneButtonRedThemes_tvMessage);
			tvHeader = (TextView) view.findViewById(R.id.DialogInfoOneButtonRedThemes_tvHeader);
			
			btButton1 = (Button) view.findViewById(R.id.DialogInfoOneButtonRedThemes_btButton1);			
		}
		
		public void initUI(){
			tvHeader.setText(Html.fromHtml(strTitle));
			tvMessage.setText(Html.fromHtml(strMessage));
			
			btButton1.setText(Html.fromHtml(strButton1));			
			btButton1.setOnClickListener(oclButton1);
		}	
	} 
	
	public class DialogHtmlInfoOneButton{
		private LayoutInflater layoutInflater;
		private Activity activity;
		
		private int boxMode;
		
		private View view;
		private TextView tvHeader;
		private WebView wvMessage;
		private Button btButton1;
		
		private String strTitle, strMessage, strButton1;
		private OnClickListener oclButton1;		
		
		public DialogHtmlInfoOneButton(Activity activity, 
				String title, String message,
				String textButton1,
				View.OnClickListener onClickListenerButton1,
				int boxMode){
			this.activity = activity;
			
			this.strTitle = title;
			this.strMessage = message;
			this.boxMode = boxMode;
			
			this.strButton1 = textButton1;
			
			this.oclButton1 = onClickListenerButton1;
		}
		
		public DialogHtmlInfoOneButton(Activity activity, int boxMode){
			this.boxMode = boxMode;
			this.activity = activity;
		}
		
		public DialogHtmlInfoOneButton(Activity activity, 
				String title, String message,
				String textButton1, int boxMode){
			this.activity = activity;
			
			this.strTitle = title;
			this.strMessage = message;
			
			this.strButton1 = textButton1;
			
			this.oclButton1 = new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					alertDialog.dismiss();
				}
			};
			
			this.boxMode = boxMode;
		}
		
		public AlertDialog getDialogInfoOneButtons(){
			layoutInflater = activity.getLayoutInflater();
			
			view = layoutInflater.inflate(R.layout.dialog_html_info_one_button_red_themes, null);
			
			initDialog();
			initUI();
			
			builder.setView(view);		
			alertDialog = builder.create();
					
			return alertDialog;
		}
				
		public DialogHtmlInfoOneButton setActivity(Activity activity) {
			this.activity = activity;
			return this;
		}

		public DialogHtmlInfoOneButton setPositiveButtonListener(OnClickListener oclButton1) {
			this.oclButton1 = oclButton1;
			return this;
		}

		public DialogHtmlInfoOneButton setStrTitle(String strTitle) {
			this.strTitle = strTitle;
			return this;
		}

		public DialogHtmlInfoOneButton setStrMessage(String strMessage) {
			this.strMessage = strMessage;
			return this;
		}

		public DialogHtmlInfoOneButton setStrButton1(String strButton1) {
			this.strButton1 = strButton1;
			return this;
		}

		public void show(){
			alertDialog = getDialogInfoOneButtons();
			alertDialog.show();
		}
				
		public void dismiss(){
			alertDialog.dismiss();
		}
		
		public void initDialog(){
			wvMessage = (WebView) view.findViewById(R.id.DialogHtmlInfoOneButtonRedThemes_tvMessage);
			tvHeader = (TextView) view.findViewById(R.id.DialogHtmlInfoOneButtonRedThemes_tvHeader);
			
			btButton1 = (Button) view.findViewById(R.id.DialogHtmlInfoOneButtonRedThemes_btButton1);			
		}
		
		public void initUI(){ 			
			switch(boxMode){
				case C.box_info :
					tvHeader.setText(Html.fromHtml(strTitle.length() > 0 ? strTitle : "[i] INFORMATION"));
					tvHeader.setBackgroundResource(R.color.app_background_dark_grey);
					btButton1.setBackgroundResource(R.drawable.bt_grey_gradient);
					btButton1.setTextColor(this.activity.getResources().getColor(R.color.app_text_black));
					break;
				case C.box_warning :
					tvHeader.setText(Html.fromHtml(strTitle.length() > 0 ? strTitle : "[!] WARNING"));
					tvHeader.setBackgroundResource(R.color.app_background_orange);
					btButton1.setBackgroundResource(R.drawable.bt_red_gradient);
					btButton1.setTextColor(this.activity.getResources().getColor(R.color.app_text_white));
					break;
				case C.box_error :
					tvHeader.setText(Html.fromHtml(strTitle.length() > 0 ? strTitle : "<b>[X] ERROR</b>"));
					tvHeader.setBackgroundResource(R.color.app_background_red);
					btButton1.setBackgroundResource(R.drawable.bt_red_gradient);
					btButton1.setTextColor(this.activity.getResources().getColor(R.color.app_text_white));
					break;
			}
			
			wvMessage.loadData(strMessage, "text/html", "UTF-8");
			
			btButton1.setText(Html.fromHtml(strButton1));			
			btButton1.setOnClickListener(oclButton1);
		}	
	} 
	// EndRegion
	
	// StartRegion Soap::Sales

	// EndRegion

	// StartRegion Soap::Supervisor

	// EndRegion

	// StartRegion IceCream::Sales

	// EndRegion

	// StartRegion IceCream::Supervisor

	// EndRegion
}
